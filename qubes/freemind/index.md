# _tensaiji / tutos / qubes / __freemind___

This tutorial will help you to install ___[FreeMind](https://freemind.sourceforge.io/wiki/)___ on ___[Qubes OS](https://www.qubes-os.org/)___ using a _Debian_ qube.

![WTFPL Badge](../../.ressources/wtfpl-badge.png)

- [Install requirements](#install-requirements)
- [Install _FreeMind_](#install-freemind)
- [Create a desktop shortcut](#create-a-desktop-shortcut)

---

## Install requirements

1. Make sure _Java_ is installed on your qube : __open a terminal and type `java --version`__. If it's already installed, skip to [_FreeMind_ installation](#install-freemind).
2. To __install the _Java Runtime Environment___, use `sudo apt install -y default-jre`.

## Install _FreeMind_

1. __[Download](https://sourceforge.net/projects/freemind/files/latest/download) _FreeMind_ binaries__ and place it wherever in the qube.
2. To __unzip the archive__, use `sudo unzip -d /opt/freemind freemind-bin-max-1.0.1.zip`.
3. Make the __main script runnable__ with `chmod +x /opt/freemind/freemind.sh`.
4. Add an __easy-access shortcut__ to the script using `sudo ln -s /opt/freemind/freemind.sh /usr/bin/freemind`. You can now __launch _FreeMind_ by typing `freemind` in a terminal__.

## Create a desktop shortcut

1. To use the _FreeMind_'s icon, __download it from [here](./freemind.png)__ and place it in the `/opt/freemind` directory.
2. __[Download](./freemind.desktop) the desktop file__ and place it in `/usr/share/applications`.
3. In a ___dom0_ terminal__, synchronize the shortcuts with `qvm-sync-appmenus <qube name>`. You should now be able to __add it to the applications menu__ in the qube's settings.
