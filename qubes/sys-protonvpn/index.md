# _tensaiji / tutos / qubes / __sys-protonvpn___

This tutorial will help you to create a ___Fedora_ netqube providing a VPN connection__ using _[ProtonVPN](https://protonvpn.com/)_.

![WTFPL Badge](../../.ressources/wtfpl-badge.png)

- [Create a new netqube](#create-a-new-netqube)
- [Install _ProtonVPN_](#install-protonvpn)
  - [Using the CLI](#using-the-cli)
  - [Using the desktop app](#using-the-desktop-app)
- [Starting the VPN automatically](#starting-the-vpn-automatically)
- [Boosting security](#boosting-security)
  - [Create a VPN firewall](#create-a-vpn-firewall)
  - [Use it by default](#use-it-by-default)

---

## Create a new netqube

1. In a ___dom0_ terminal__, use `qvm-create --standalone --template fedora-36 --label purple sys-vpn` to create a ___Fedora_-based standalone qube__. This may take some times...
2. Depending on your desired configuration, use one of this command to __set the network provider__ for our qube :
   1. `qvm-prefs --set sys-vpn netvm sys-firewall` to use your __firewall configuration__ ;
   2. `qvm-prefs --set sys-vpn netvm sys-net` to bypass your firewall (___not recommended___).
3. __Make it a netqube__ using `qvm-prefs --set sys-vpn provides_network true`.

## Install _ProtonVPN_

### Using the CLI

1. Open a terminal (_in the VPN netqube_) and __install the _ProtonVPN_ CLI__ using `sudo dnf install -y protonvpn-cli`.
2. Use `protonvpn-cli login <username>` to __setup your credentials__.

### Using the desktop app

1. Open a terminal (_in the VPN netqube_) and __download _ProtonVPN_ RPM package__ using `wget https://repo.protonvpn.com/fedora-39-stable/protonvpn-stable-release/protonvpn-stable-release-1.0.1-2.noarch.rpm`.
2. __Install _ProtonVPN_ desktop app__ by typing `sudo dnf install -y protonvpn`.
3. Use `protonvpn` to launch the application and __setup your credentials__.
4. _(Optional)_ __Remove the RPM package__ with `rm protonvpn-stable-release-1.0.1-2.noarch.rpm`.

> For further informations, see [_ProtonVPN_ official support](https://protonvpn.com/support/official-linux-vpn-fedora/).

## Starting the VPN automatically

1. To __start automatically _ProtonVPN_ on the qube startup__, you can :
   1. __[Download](./protonvpn-cli.desktop) this desktop file__ and place it in `/home/user/.config/autostart`[^autostart] if you want to use the CLI ;
   2. Create a __symbolic link to _ProtonVPN_ in the autostart folder[^autostart]__, using `ln -s /usr/share/applications/protonvpn.desktop /home/user/.config/autostart/`, if you want to use the GUI (_slower to start_).
2. In a _dom0_ terminal, type `qvm-prefs --set sys-vpn autostart true` to __start automatically the VPN netqube on _dom0_ startup__.

[^autostart]: create the `autostart` directory if doesn't exist.

---

## Boosting security

### Create a VPN firewall

1. Open a _dom0_ terminal and use `qvm-create --disp --template fedora-36-dvm --label purple sys-vpn-firewall` to create a __VPN firewall netqube__.
2. Type `qvm-prefs --set sys-vpn-proton-vpn netvm sys-vpn` to use the ___VPN qube_ as network provider__.
3. To __make it a netqube__, use `qvm-prefs --set sys-vpn-firewall provides_network true`.

### Use it by default

1. To use it as __default network provider__, type `qubes-prefs --set default_netvm sys-vpn-firewall` in a _dom0_ terminal.
2. Make it __start automatically on _dom0_ startup__ using `qvm-prefs --set sys-vpn-firewall autostart true`.
