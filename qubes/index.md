# _tensaiji / tutos / __qubes___

This folder contains tutorials concerning ___[Qubes OS](https://www.qubes-os.org/)___.

![WTFPL Badge](../.ressources/wtfpl-badge.png)

## Tutorials

- ___[freemind](./freemind/) :__ installing __[FreeMind](https://freemind.sourceforge.io/wiki/)__._
- ___[sys-protonvpn](./sys-protonvpn/) :__ creating a __[ProtonVPN](https://protonvpn.com/) netqube__._
